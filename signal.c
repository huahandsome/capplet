	#include <stdio.h> 
	#include <signal.h>
	#include <unistd.h>

	int ctrl_c_count = 0;
	void (* old_handler)(int); 
	void ctrl_c(int);

	int main()
	{
		int c;
        
        // old_handler is a *Original SIGINT* function, to handle signal SIGINT. 
        // The behavior of this function, is to exit the program.
        // Now, in this program, we re-defined the behavior of the SIGINT handler with
        // function ctrl_c, so after do that, we restore the old SIGINT hanlder 
        
		old_handler = signal(SIGINT, ctrl_c);
	
		while ((c = getchar())!='\n'); 
			printf("ctrl-c count = %d\n", ctrl_c_count);

		(void) signal(SIGINT, old_handler);

		for (;;);

        return 0;
	}



	void ctrl_c(int signum) 
	{
		(void) signal(SIGINT, ctrl_c); 
		++ctrl_c_count;
	}
